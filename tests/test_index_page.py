import requests
import config

class TestIndexPage:
    """Tests for Index Page: """
    def test_index_page_is_loaded(self):
        """Verify that index page content is loaded correctly"""
        response = requests.get(config.base_url)
        # Then
        assert response.status_code == 200
        assert 'Hello, World!' in response.text
