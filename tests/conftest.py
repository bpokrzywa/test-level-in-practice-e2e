import pytest
import requests
from faker import Faker

import endpoints

fake = Faker()

def pytest_itemcollected(item):
    par = item.parent.obj
    node = item.obj
    pref = par.__doc__.strip() if par.__doc__ else par.__class__.__name__
    suf = node.__doc__.strip() if node.__doc__ else node.__name__
    if pref or suf:
        item._nodeid = ' '.join((pref, suf))

@pytest.fixture
def new_user_data() -> dict:
    expected_email = fake.safe_email()
    create_user_payload = {'email': expected_email}

    create_user_response = requests.post(endpoints.users, json=create_user_payload)

    assert create_user_response.status_code == 201
    create_user_response_json = create_user_response.json()
    assert create_user_response_json['email'] == expected_email
    assert 'id' in create_user_response_json

    return {'email': expected_email, 'id': create_user_response_json['id']}

